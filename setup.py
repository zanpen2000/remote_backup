from distutils.core import setup

setup(
    name='remote_backup',
    version='1.0',
    packages=['main', 'main.units', 'main.functions', 'main.functions.backup_plugins',
              'main.functions.backup_plugins.plugins_tests', 'remote_backup_config'],
    url='',
    license='',
    author='peng',
    author_email='zanpen2000@gmail.com',
    description=''
)
