# MySql 学习笔记

## 安装

    服务端
    sudo apt-get install mysql-server

    客户端
    sudo apt-get install mysql-client-core-5.6


## 登陆

    $mysql -h 192.168.2.79 -u root -p

    提示:
        $ERROR 2003 (HY000): Can't connect to MySQL server on '192.168.2.79' (111)

    Google说:

        >MySQL by default listens only on the localhost (127.0.0.1) interface.

    so:

        在配置文件中解除如下代码的注释：

            # Instead of skip-networking the default is now to listen only on
            # localhost which is more compatible and is not less secure.
            # bind-address          = 127.0.0.1

        >Uncomment the bind-address line, and bind to 0.0.0.0 to bind to all addresses on the system.

    then:
        sudo service mysql restart

    and again:
        mysql -h 192.168.2.79 -u root -p

    prompt:
        >ERROR 1130 (HY000): Host '192.168.2.44' is not allowed to connect to this MySQL server

    on server, login to mysql server:

        $mysql -u root -p

        and:
            mysql>GRANT ALL privileges on *.* to 'root'@'%';
            mysql>flush privileges;

    on client mathine:

        $mysql -h 192.168.2.79 -u root -p
        >Enter password:******
        >ERROR 1045 (28000): Access denied for user 'root'@'192.168.2.44' (using password: YES)

    but empty password can login:

        $mysql -h 192.168.2.79 -u root

    所以在服务器端登陆mysql， 使用如下指令：

        mysql>CREATE USER 'peng'@'192.168.2.44' IDENTIFIED BY '123123';
        mysql>grant all privileges on *.* to 'peng'@'192.168.2.44';
        mysql>flush privileges;

    然后就可以登陆了.

    疑问：

        使用如下指令不能解决该问题，不知道为什么

        mysql>grant all privileges on *.* to 'root'@'%' with grant option;


## 创建数据库

    mysql> create database backup_test character set gbk;
    mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | backup_test        |
    | mysql              |
    | performance_schema |
    +--------------------+
    4 rows in set (0.00 sec)

## 建表

    mysql> use backup_test
    Database changed
    mysql> create table employee
        -> (
        -> id int unsigned not null auto_increment primary key,
        -> name char(8) not null,
        -> sex char(4) not null,
        -> age tinyint unsigned not null,
        -> tel char(13) null default "-"
        -> );
    Query OK, 0 rows affected (0.00 sec)

    mysql> show tables;
    +-----------------------+
    | Tables_in_backup_test |
    +-----------------------+
    | employee              |
    +-----------------------+
    1 row in set (0.00 sec)

    mysql>INSERT INTO `employee` VALUES (1,'张鹏','男',38,'15613539902');
    mysql> select * from employee;
    +----+--------+-----+-----+-------------+
    | id | name   | sex | age | tel         |
    +----+--------+-----+-----+-------------+
    |  1 | 张鹏   | 男  |  38 | 15613539902 |
    +----+--------+-----+-----+-------------+
    1 row in set (0.01 sec)

## 备份

### 增量备份

    1. 首先完整备份

	        $ mysqldump -h 192.168.2.79 -u peng -p --single-transaction --master-data=2 backup_test > test.sql

	        $ Binlogging on server not active

        修改 my.cnf (/etc/mysql/my.cnf),  在 [mysqld] 小节最后加入一行：

            log-bin=mysql-bin

        然后重启mysql:
	        $sudo service mysql restart

    2. 增加记录

        mysql>INSERT INTO `employee`(name, sex, age) VALUES ('刘艳丽','女',25);
        mysql>INSERT INTO `employee`(name, sex, age) VALUES ('张磊','男',32);
        mysql>flush logs;

### 备份补充

    $mysqldump -h 192.168.2.79 -upeng -p123123 backup_test > test.sql

### 恢复

    $mysqldump -h 192.168.2.79 -udykj -p123123 backup_test < test.sql
