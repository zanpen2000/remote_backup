### 通过Docker源安装最新版本通过Docker源安装最新版本

    要安装最新的 Docker 版本，首先需要安装 apt-transport-https 支持，之后通过添加源来安装。要安装最新的 Docker 版本，首先需要安装 apt-transport-https 支持，之后通过添加源来安装。

    $ sudo apt-get install apt-transport-https
    $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
    $ sudo bash -c "echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
    $ sudo apt-get update
    $ sudo apt-get install lxc-docker

### 启动docker服务

    $ sudo service docker start

### 获取镜像

    $ sudo docker pull ubuntu:14.04

*有时候官方仓库注册服务器下载较慢，可以从其他仓库下载。 从其它仓库下载时需要指定完整的仓库注册服务器地址。例如*

    $ sudo docker pull dl.dockerpool.com:5000/ubuntu:12.04
    Pulling dl.dockerpool.com:5000/ubuntu
    ab8e2728644c: Pulling dependent layers
    511136ea3c5a: Download complete
    5f0ffaa9455e: Download complete
    a300658979be: Download complete
    904483ae0c30: Download complete
    ffdaafd1ca50: Download complete
    d047ae21eeaf: Download complete

### 创建一个容器，让其中运行 bash 应用创建一个容器，让其中运行 bash 应用

    $ sudo docker run -t -i ubuntu:14.04 /bin/bash

 ### 列出本地镜像

    $ sudo docker images //显示本地已有的镜像

### 修改已有镜像

    $ sudo docker commit -m "注释" -a "提交者" 0b2616b0e5a8 ubuntu:14.04

    1. 其中"0b2616b0e5a8"是镜像id
    2. "-m" 参数后面跟提交注释
    3. "-a" 参数后面跟提交者的信息如名字
    4. "ubuntu:14.04" 代表仓库和版本号(tag)
