#coding:utf-8
from django.test import TestCase
from djcelery import models as celery_models
from django.conf import settings

class task_test(TestCase):

    def setUp(self):
        self.settings = settings

    def test_execute_backup_task_with_name_by_task(self):
        '''动态获取任务实例，并将任务和该任务的schedule动态塞入celery队列
        参考：http://my.oschina.net/kinegratii/blog/292395?fromerr=clIv9dwt（djcelery实现定时任务）

        如文档所述，填充 djcelery 的模型 djcelery_crontabschedule 即可获得可后台运行的任务
        这里只需要填充 djcelery_periodicTask 表即可。
        '''
        settings = {"source_path":"~/sources/swift", "target_path":"~/sources/swift_copy"}

        items =celery_models.IntervalSchedule.objects.all()
        print("items: ",items) #这里得到的是空列表

        interval = celery_models.IntervalSchedule.objects.first()

        r = celery_models.PeriodicTask.objects.update_or_create(name='test_local_file_backup', task='backup_task', \
                                                           kwargs=settings, \
                                                           enabled=True, description='there is no description for this task.')
        r.interval = interval
        r.save()
        print(r)
        print(interval)

        rr = celery_models.IntervalSchedule.objects.get(id=4)
        print(rr)


    #def test_backup_mysql(self):
    #    rst = backup_mysql.apply().get()
    #    self.assertEqual(rst, 0)

    # def test_execute_backup_task_with_name(self):
    #     '''这里只是获取到任务的实例，给实例赋值并运行，并没有实现任务的task化'''
    #     backup_class = get_backup_task.apply(args=('mysql-backup',)).get()
    #     r = backup_class(host='192.168.2.79', port=3306,  user='dykj', pwd='123123', database='backup_test', output='test.sql')
    #     result = r.run()
    #     self.assertEqual(result, 0)