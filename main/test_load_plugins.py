import os

from django.test import TestCase

import main.functions.backup_plugins as plugins
from main.functions.load_plugins import get_plugins
from units.interfaces import ArgumentError, Backup

pathname = os.path.dirname(plugins.__file__)


class load_plugins_tests(TestCase):
    def test_load_plugins_with_empty_path(self):
        pathname = ''
        interface = Backup
        self.assertRaises(OSError, get_plugins, pathname, interface)

    def test_load_plugins_with_path_which_not_exists(self):
        pathname = '/home/peng/123'
        interface = Backup
        self.assertRaises(OSError, get_plugins, pathname, interface)

    def test_load_plugins_with_None_interface(self):
        self.assertRaises(ArgumentError, get_plugins, pathname, None)
        self.assertRaises(ArgumentError, get_plugins, pathname, '')

    def test_load_plugins_with_interface_which_str(self):

        interface = 'teststr'
        self.assertRaises(TypeError, get_plugins, pathname, interface)

    def test_load_plugins_with_list_result(self):

        got_plugins = get_plugins(pathname, Backup)

        self.assertIsNotNone(got_plugins)
        self.assertEqual(len(got_plugins), 7)
