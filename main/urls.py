from django.conf.urls import url

import views

urlpatterns = [
    url(r'^settings/del/(?P<id>\d+)/$', views.settings_del, name='settings_del'),
    url(r'^settings/$', views.settings,name='settings'),
    url(r'^plugins/$', views.plugins,name='plugins'),
    url(r'^djcelery_interval_schedule/$', views.djcelery_interval_schedule, name='djcelery_interval_schedule'),
    url(r'^djcelery_crontab_schedule/$', views.djcelery_crontab_schedule, name='djcelery_crontab_schedule'),
    url(r'^djcelery_periodictask/$', views.djcelery_periodictask, name='djcelery_periodictask'),
    url(r'^djcelery_interval_schedule/del/(?P<id>\d+)$', views.djcelery_interval_schedule_del, name='djcelery_interval_schedule_del'),
    url(r'^djcelery_crontab_schedule/del/(?P<id>\d+)$', views.djcelery_crontab_schedule_del, name='djcelery_crontab_schedule_del'),
    url(r'^djcelery_periodictask/del/(?P<id>\d+)$', views.djcelery_periodictask_del, name='djcelery_periodictask_del'),


    url(r'^settings/edit/(?P<id>\d+)?/$', views.settings_edit, name='settings_edit'),
    url(r'^djcelery_interval_schedule/edit/(?P<id>\d+)?$', views.djcelery_interval_schedule_edit, name='djcelery_interval_schedule_edit'),
    url(r'^djcelery_crontab_schedule/edit/(?P<id>\d+)?$', views.djcelery_crontab_schedule_edit, name='djcelery_crontab_schedule_edit'),
    url(r'^djcelery_periodictask/edit/(?P<id>\d+)?$', views.djcelery_periodictask_edit, name='djcelery_periodictask_edit'),
    url(r'^$', views.homePage,name='home'),

    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/login/'}),
    url(r'^password_change/$','django.contrib.auth.views.password_change'),
    url(r'^password_change_done/$', 'django.contrib.auth.views.password_change_done'),
    url(r'^password_reset/$','django.contrib.auth.views.password_reset'),
    url(r'^password_reset_done/$','django.contrib.auth.views.password_reset_done'),

    url(r'^task_result/detail/(?P<id>\d+)$',views.task_result_detail, name='task_result_detail'),

    url(r'^task_results/$', views.task_results.as_view(), name='task_results'),
]
