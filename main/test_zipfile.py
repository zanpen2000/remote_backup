from django.test import TestCase

# Create your tests here.

from units.zipfiles import create


class zipfiles_module_tests(TestCase):
    def test_zipfiles_empty_args(self):
        outfile = ''
        files = ''
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_doesnotexists_infile(self):
        files = '/home/peng/Android/11.txt'
        outfile = 'test.zip'
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_doesnotexists_inpath(self):
        files = '/home/peng/Android_not_exists'
        outfile = 'test.zip'
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_empty_list(self):
        files = ()
        outfile = 'test.zip'
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_invalid_value_in_list1(self):
        files = (0, '111')
        outfile = 'test.zip'
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_invalid_value_in_list2(self):
        files = ('222', '111')
        outfile = 'test.zip'
        self.assertRaises(ValueError, create, files, outfile)

    def test_zipfiles_already_exists_outfile(self):
        files = __file__
        outfile = 'test.zip'
        result = create(files, outfile)
        self.assertEqual(result, True)
