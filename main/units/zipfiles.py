#!/usr/bin/env python
#coding:utf-8

import zipfile
import functools
import os

def check_input_args(zipmethod):

    @functools.wraps(zipmethod)
    def wrapper(prefiles, outfile):
        if not prefiles or not isinstance(outfile, str):
            raise ValueError('input arguments can not be None.')
        if isinstance(prefiles, str) and not os.path.exists(prefiles):
            raise ValueError('input file or path does not exists.')
        if hasattr(prefiles, '__iter__'):
            if any([not isinstance(item, str) for item in prefiles]) or any([not os.path.exists(item) for item in prefiles]) or not all(prefiles):
                raise ValueError('input list/tuple has invalid value.')

        return zipmethod(prefiles, outfile)

    return wrapper

@check_input_args
def create(prefiles, outfile):
    """accept path or file list to zip to a single 7z file."""
    if isinstance(prefiles, str):
        return _create_by_file_or_path(prefiles, outfile)
    elif hasattr(prefiles, '__iter__'):
        return all(_create_by_file_or_path(f, outfile) for f in prefiles)

def _create_by_file_or_path(file_or_path, outfile):
    zfile = zipfile.ZipFile(outfile, 'w', zipfile.ZIP_DEFLATED)
    if os.path.isdir(file_or_path):
        for p, d, f in os.walk(file_or_path):
            for fname in f:
                zfile.write(os.path.join(p, fname))
    elif os.path.isfile(file_or_path):
        zfile.write(file_or_path)
    zfile.close()
    return os.path.exists(outfile)
