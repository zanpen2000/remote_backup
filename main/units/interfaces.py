#!/usr/bin/env python
#coding:utf-8

class Backup(object):
    """the interface of backup database or files
    we need use this interface for plugins.
    """

    name = None
    desc = ''

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs

        if kwargs.has_key('name'):
            self.name = kwargs.get('name')

        if kwargs.has_key('desc'):
            self.desc = kwargs.get('desc')

    def __call__(self):
        pass

    def execute(self):
        """按照设定的调度信息执行任务"""
        pass

class ArgumentError(ValueError):
    pass
