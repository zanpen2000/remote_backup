class Menu(object):
    def __init__(self, name, cls):
        self.menu_items = []
        self.name = name
        self.cls = cls

    def add_item(self, name, cls):
        for i in self.menu_items:
            if i.name == name:
                raise Exception('already exist')
        self.menu_items.append(Menu(name, cls))

    def __str__(self):
        if self.name is None:
            name = "TOP"
        else:
            name = self.name

        return "menu {} children {}".format(name, ' * '.join(['({} {})'.format(i.name, i.cls.__name__) for i in self.menu_items])) + '\n' + ''.join([str(i) for i in self.menu_items])


my_menu = Menu(None, None)

def menu(name, parent_menu=None):

    def inner(cls):
        m = find_menu_by_name(my_menu, parent_menu)
        m.add_item(name, cls)

        return cls

    def find_menu_by_name(curr_menu, name):
        if name is None:
            return curr_menu
        for i in curr_menu.menu_items:
            if i.name == name:
                return i
            found = find_menu_by_name(i, name)
            if found:
                return found

        return None

    return inner


@menu("a", parent_menu=None)
class menuItem(object):
    pass

@menu("b", parent_menu="a")
class menuItem2(object):
    pass

@menu("c", parent_menu="b")
class menuItems3(object):
    pass

print my_menu
