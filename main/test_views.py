# coding:utf-8
from __future__ import unicode_literals
from django.test import TestCase

class ViewTest(TestCase):

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

    def test_task_results(self):
        response = self.client.get('/task_results/')
        self.assertEqual(response.status_code, 302)



