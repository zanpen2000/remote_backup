#!/usr/bin/env python
# coding:utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class AppSettings(models.Model):
    """系统设置表"""
    name = models.CharField(max_length=200, verbose_name='设置名称')
    value = models.CharField(max_length=2000, verbose_name='设置值')
    desc = models.CharField(max_length=2000, verbose_name='描述')
    lastEditor = models.ForeignKey(User, verbose_name='用户')

    class Meta:
        app_label = 'main'
        db_table = 'main_AppSettings'
        verbose_name = '系统设置'
        verbose_name_plural = '系统设置'

    def __unicode__(self):
        return self.name


class TaskResults(models.Model):
    """存储Task执行结果"""
    task_id = models.CharField(max_length=200, verbose_name='任务ID')
    task_name = models.CharField(max_length=200, verbose_name='任务名称')
    plugin_name = models.CharField(max_length=200, verbose_name='插件名称')
    retval = models.CharField(max_length=200, verbose_name="返回值")
    kwargs = models.TextField(verbose_name="参数")
    status = models.CharField(max_length=200, verbose_name='状态')

    class Meta:
        app_label = 'main'
        db_table = 'main_TaskResults'
        verbose_name='任务执行结果'
        verbose_name_plural = '任务执行结果'

    def __unicode__(self):
        return "{} on {} ".format(self.plugin_name, self.task_name)
