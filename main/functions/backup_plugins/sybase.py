#!/usr/bin/env python
# coding:utf-8

import json
from main.units.interfaces import ArgumentError, Backup

class SybaseBackup(Backup):

    """backup sybase database"""

    # default task name
    name = 'sybase-database-backup'

    # default task description
    desc = 'dump sybase database to disk.'

    def __init__(self, *args, **kwargs):
        super(SybaseBackup, self).__init__(*args, **kwargs)

    def execute(self):
        '''need sybase client'''
        pass

