#!/usr/bin/env python
# coding:utf-8

import json
from main.units.interfaces import ArgumentError, Backup

class MongodbBackup(Backup):

    """backup Mongodb database"""

    # default task name
    name = 'Mongodb-database-backup'

    # default task description
    desc = 'dump Mongodb database to disk.'

    def __init__(self, *args, **kwargs):
        super(MongodbBackup, self).__init__(*args, **kwargs)

    def execute(self):
        """use mongodump to backup, sudo apt-get install mongodb-client,
        http://www.jb51.net/article/40285.htm
        """
        pass

