#!/usr/bin/env python
# coding:utf-8

import json
from main.units.interfaces import ArgumentError, Backup

class FtpFilesBackup(Backup):

    """backup files"""

    # default task name
    name = 'ftp-files-backup'

    # default task description
    desc = 'files backup from a ftp server to other.'

    def __init__(self, *args, **kwargs):
        super(FtpFilesBackup, self).__init__(*args, **kwargs)

    def execute(self):
        """
        Windows下路径的设置和LInux下路径的设置有差别，请注意
        Windows下 f:\\backup 应为 /cygdrive/f/backup
        Linux 下 无变化

        """
        try:
            host = self.kwargs.get('host')
            port = self.kwargs.get('port')
            user = self.kwargs.get('user')
            pwd = self.kwargs.get('pwd')
            local_path = self.kwargs.get('local_path')
            remote_path=self.kwargs.get('remote_path')
            lftp_path = self.kwargs.get('lftp_path')

            lftp_script = "%s -u %s,%s -e 'mirror --only-newer --verbose --use-pget-n=8 -c %s %s ;quit' %s:%s"
            cmd = lftp_script % (lftp_path, user, pwd, remote_path, local_path, host, port)

        except Exception as exception:
            print ("Unexpected error:", exception.message)
        else:
            import subprocess
            return subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).communicate()
