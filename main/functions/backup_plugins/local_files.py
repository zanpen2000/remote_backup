#!/usr/bin/env python
# coding:utf-8

import json
from main.units.interfaces import ArgumentError, Backup

class LocalFilesBackup(Backup):

    """backup local files, use rsync, need 'rsync' to install."""

    # default task name
    name = 'local_files_backup'

    # default task description
    desc = 'files backup on local server.'

    def __init__(self, *args, **kwargs):
        super(LocalFilesBackup, self).__init__(*args, **kwargs)

    def execute(self):
        """按照设定的调度信息执行任务"""
        src = self.kwargs.get('source_path')
        target = self.kwargs.get('target_path')

        import subprocess
        return subprocess.Popen(['rsync','-a', src, target]).communicate()
