#!/usr/bin/env python
# coding:utf-8

import json
from main.units.interfaces import ArgumentError, Backup

class OracleBackup(Backup):

    """backup oracle database"""

    # default task name
    name = 'oracle-database-backup'

    # default task description
    desc = 'dump oracle database to disk.'

    def __init__(self, *args, **kwargs):
        super(OracleBackup, self).__init__(*args, **kwargs)

    def execute(self):
        """exp system/manager@dycms inctype=complete file=d:\\20160325.dmp full=y"""
        pass
