import json

from django.test import TestCase

from main.functions.backup_plugins.ftp_files import FtpFilesBackup
from main.units.interfaces import ArgumentError

class FtpFilesTests(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_run(self):
        ftpbackup = FtpFilesBackup(host='192.168.2.123', port=21, user='administrator', pwd='dykjhz123456@', local_dir='~/test', remote_path='/pics')
        result = ftpbackup.run()
        self.assertEqual(result, 0)

    def test_run_arg_error(self):
        pass
