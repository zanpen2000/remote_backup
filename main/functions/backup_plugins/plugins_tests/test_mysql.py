import json

from django.test import TestCase

from main.functions.backup_plugins import mysql
from main.models import BackupTask, Groups, OperationLevel, Users
from main.units.interfaces import ArgumentError


class MySqlTests(TestCase):
    def setUp(self):
        level = OperationLevel()
        level.name = 'test'
        level.desc = 'test'
        level.save()

        group = Groups()
        group.name = 'test_group'
        group.level = level
        group.desc = 'test group'
        group.save()

        user = Users()
        user.name = 'zp'
        user.phone = ''
        user.pwd = '123456'

        user.group = group

        user.save()

        self.user = user

    def tearDown(self):
        del self.user


    # def test_run(self):
    #     addr = '192.168.2.79'
    #     mb = mysql.MySqlBackup(host=addr,
    #                            port=3306,
    #                            user='dykj',
    #                            pwd='123123',
    #                            database='backup_test',
    #                            output='test.sql')
    #     e = mb.run()
    #     self.assertEqual(e, 0)

    def test_run_arg_error(self):
        mb = mysql.MySqlBackup()
        self.assertRaises(ArgumentError, mb.run)
