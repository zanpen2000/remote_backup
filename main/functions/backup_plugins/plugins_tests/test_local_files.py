#!/usr/bin/env python
#coding:utf-8

import json

from django.test import TestCase

from main.functions.backup_plugins.local_files import LocalFilesBackup
from main.units.interfaces import ArgumentError

import os
import tempfile

class LocalFilesBackupTest(TestCase):
    """
        1、获取临时目录，需要写权限
        2、建立两个目录A和B
        3、A目录临时生成临时文件
        4、将A目录和B目录同步
        5、比较两个目录文件
    """

    def setUp(self):
        self.temp_a = tempfile.mkdtemp()
        self.temp_b = tempfile.mkdtemp()

        for i in range(100):
            tempfile.mkstemp(dir=self.temp_a)

        self.init_files_list =  os.listdir(self.temp_a)

    def tearDown(self):
        print '\nremoving tmp dirs...',
        try:
            import shutil
            shutil.rmtree(self.temp_a)
            shutil.rmtree(self.temp_b)
        except:
            raise

        print('done.')

    def test_run(self):
        backup = LocalFilesBackup(source_path=self.temp_a+'/', target_path=self.temp_b)
        p = backup.run()

        self.assertEqual(p, 0)
        self.assertItemsEqual(self.init_files_list, os.listdir(self.temp_b))
