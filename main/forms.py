# coding:utf-8
from __future__ import unicode_literals
from django import forms
from django.forms import ModelForm
from djcelery import models as celery_models

from main.functions.load_plugins import get_plugins_choices_list
from main.models import AppSettings, TaskResults

plugins = get_plugins_choices_list()


class djcelery_intervalschedule_form(ModelForm):
    id = forms.IntegerField(label="序号", widget=forms.HiddenInput(attrs={'value': ''}), required=False)

    class Meta:
        model = celery_models.IntervalSchedule
        fields = '__all__'


class djcelery_crontabschedule_form(ModelForm):
    id = forms.IntegerField(label="序号", widget=forms.HiddenInput(attrs={'value': ''}), required=False)

    class Meta:
        model = celery_models.CrontabSchedule
        fields = '__all__'


class djcelery_periodictask_form(ModelForm):
    id = forms.IntegerField(label="序号", widget=forms.HiddenInput(attrs={'value': ''}), required=False)

    class Meta:
        model = celery_models.PeriodicTask
        exclude = ['queue', 'exchange', 'routing_key', 'last_run_at', 'total_run_count', 'date_changed']


class djcelery_periodictasks_form(ModelForm):
    class Meta:
        model = celery_models.PeriodicTasks
        fields = '__all__'


class AppSettingsForm(ModelForm):
    id = forms.IntegerField(label="序号", widget=forms.HiddenInput(attrs={'value': ''}), required=False)

    class Meta:
        model = AppSettings
        fields = '__all__'


class TaskResultsForm(ModelForm):
    class Meta:
        model = TaskResults
        fields = '__all__'
