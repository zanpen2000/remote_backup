from django.contrib import admin
from models import *


class AppSettingsAdmin(admin.ModelAdmin):
    list_display = ('name', 'value', 'desc', 'lastEditor')

admin.site.register(AppSettings)

from kombu.transport.django import  models as kombu_models

admin.site.register(kombu_models.Message)
admin.site.register(kombu_models.Queue)

admin.site.register(TaskResults)
